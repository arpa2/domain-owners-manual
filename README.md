# Domain Owner's Manual

**Tao of Online Self-Sufficiency and Sovereignity.**

> *This is a manual for self-sufficiency on the
> Internet.  It explains an InternetWide Architecture
> and how this can be used to control online identity
> and the ways others can use it.*

This manual is the result of the open source development project of an open architecture for the entire Internet, as reported about at [InternetWide.org](http://internetwide.org).  The information below explains how you can use open protocols that were designed for self-sufficiency for any Internet user, ranging from indivuals or families to large multi-national organisations.  Most of what you will find here is founded on a domain name.  And even when you already own a domain, this domain owner's manual is likely to teach you to improve how you can *be yourself* on the Internet.

The software implementing these ideas sometimes calls for software; the ARPA2 projects always delivers open source software and often extends existing open source software.  Where existing protocols are modified, their specifications are fully public, and their broad implementation by others is strongly encouraged.  The projects making such implementations are the various ARPA2 work projects that together enable our InternetWide Architecture.

**Problems solved:**

  * Establish online identity and privacy &mdash; before getting active
  * Login in the morning &mdash; benefit for the rest of the day
  * Get yourself a domain &mdash; and control `user@domain.name` identities
  * Bring Your Own IDentity (BYOID) &mdash; also to foreign servers
  * Use light-weight aliases &mdash; and control who may contact each
  * Use bootstrap identities &mdash; and forego password reset emails

```
Copyright 2020 Rick van Rein <rick@openfortress.nl>, for InternetWide.org
SPDX-License-Identifier: CC-BY-SA-4.0
```


## This is Why: Guiding Principles

We first explain, briefly, the guiding principles behind our designs.  *This explains the "why" of our designs; you are welcome to skip this initially.*  These principles are often implicit in the work of open source programmers, but we believe it makes sense to explain what moves our type of developer, and how it touches upon everyday user experience.  We want to inform users about the impact of their individual choices on the centralisation of the Internet, and how to move back to a system that is open and free for all.

### Why?  Be in the Line of Control

The cornerstone of your online presence is *your personal identity* which may be an email address `john@example.com` or something that looks like one.  Interesting about this structure is how it gradually delegates authority from way-up to you.  This can be helpful to understand how your online freedom is protected.

The root of the entire naming scheme for the Internet is `.` which is omnipresent and not usually shown.  This root level is controlled on behalf of the entire Internet community by general organisations.  The root level points to the generally supported top-level domains for countries and general-purpose uses; think of `nl` for the Netherlands or `com` for commercial uses.  This top-level in turn points to domain names that any individual or organisation can register, such as `example.com` in this case.  These controlling individuals or organisations are the so-called "domain owners" for the given name.  They setup further pointers to sub-domains like `people.example.com` and/or to (services that welcome) users like `sales@example.com` and `john@people.example.com` and they control whether users are welcomed, how they gain access to their services and identities, and so on.

Domains like `example.com` are open for anyone to register, and as long as a small annual fee is paid for its administration, such a domain is kept alive.  Someone losing interest in their domain can give it up, which means the name gets recycled -- another domain owner may take control of the domain name.  Very importantly, *taking away someone's registered name is not supported* by the administrative offices in charge of them.  The domain is yours, as long as you continue to pay the annual administration fee.  Also, as follows from the complete technical control, *you alone decide what to do with your domain name* and although your country is likely to impose rules of legal conduct, these local concerns are generally not enforced by the technical infrastructure of the Internet.  Finally, there is no restriction to the number of domain names you can have; it may be quite helpful to distribute control for different entities, such as different companies or perhaps just different hobbies.

Note that you are also free to welcome a community of like-minded individuals under your domain.  There is nothing special about the few well-known names that everyone seems to flock around these days; you can do similar things with your domain, and probably more focussed on your interests, as long as you have proper instruments for doing so.  Enter the InternetWide Architecture, which aims to place you at the helm of your domain name and provide you with many tools that were up to now too difficult for end-users to have.  Please note that this is an ongoing quest, and we depend on others to gradually expand the possibilities.  This is a distributed line of work, because we do not want to end up being yet another molog trying to control it all.  Please, understand that this may take time, and that you can actively participate to facilitate the things you care for.

### Why?  An Identity for every Role you Play

![Be anyone, be multiple ones, and control connections (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-4.png)

The InternetWide Architecture generally assumes that you own a domain name.  If not, go and get yourself one!  And if you have one but fear damaging it, then consider getting another for playing around with new ideas.  You will receive the full benefits of expressing yourself online in any way you want, and in the process you will learn much more about what makes the Internet tick than any webservice could ever teach you.  Once you have your domain name, you can start creating identities of your choosing, unconstrained by silly rules from silly websites.

Once you have a domain name like `example.com`, you can create as many user names like `john@example.com` as you like.  And because this is so easy, why not have many for each end user?  Presenting yourself differently for different angles on your online activity is practical and it gives you a handle to control your privacy and accessibility; you could sort your relationships by giving separate addresses to each.  This may work really well for commercial vendors; once they start sending spam you simply abolish the address they think they have of you.  If your address leaks from one vendor to another you might even be able to spot that, for instance if their name is part of the identity they got from you.

Unfortunately, governments have not seen this light yet; their identitification codes are fixed and get smeared all over the place; different traders can use this to link bits of data together to collect more knowledge about you than you might appreciate.  This is even easier with online data, so we should avoid making this same mistake.  And yes, laws are being defined to thwart that, but the Internet is international and difficult to regulate, and laws only matter to those seeking to do good.  In other words, you still want to fend for your own privacy.

Given these concerns, we made an effort to design a really good identity system for the InternetWide Architecture.  For example, if your primary identity is `john@example.com` you might add aliases, such as `john+cook@example.com` and `john+singer@example.com`.  You could create an alias like `john+magicbullets@example.com` dedicated to a vendor named Magic Bullets Ltd.  When your domain welcomes others to use an account, for instance `mary@example.com`, then they might create aliases for themselves too.  Aliases are designed as incredibly light-weight alternate names, so there is no real limit on creating as many of them as desired.

Pseudonyms are a bit heavier, but they have an advantage over aliases that they do not reveal your underlying identity.  For example, if John wants to subscribe to commercial information without being bothered on his primary address he might use `ads@example.com` for that purpose.  It gives no hint about the `john@example.com` primary entity, and care is taken that it looks as different to others as `mary@example.com` does.  Even pseudonyms can have aliases, so it is once more possible to give different addresses to different ad subscriptions.

*There are a few more forms, such as addresses with an expiration date and only addressable from certain domains or senders, but more on that later.*

### Why?  Collaboration in Groups

![Find a Place to be Together (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-6.png)

Internet is a place for collaboration.  In fact, it is *the ultimate social network*, and anything built on top of it is merely a subset of its functionality.  With that in mind, the InternetWide Architecture offers strong support for collaborative identities, namely through groups and roles, supportive of any protocol and with maximal inclusion for identities from other domains.

An example of a group could be your baseball team, which you could give a primary identity like `baseball@example.com`.  An example of a role could be the administrator(s) of your domain, identified as `admin@example.com`.  Roles and their occupants are mostly similar to groups and their members, so we will only explain groups below.  When you explore service to run under your domain, you may however find that they implement different logic for roles than for groups.

Group members are identified by way of an alias under the group, so `baseball+mary@example.com` is someone known as Mary *within the context of the group* and translated to an underlying identity when the address is contacted.  This underlying identity is not automatically `mary@example.com` or even local to the domain; it may be an alias for `marian@example.com` as easily as for an external user `mirjam.schoenmaker@domein.nl`.

Note how all group members appear with a group member alias under the group's domain, so that their underlying address is not necessary to communicate with them.  Services targeting groups are encouraged to conceal the underlying address, to protect the privacy of the members.  In case of abuse the service administrator (usually the domain owner) can trace the underlying identity, and take apropriate action.  Technically, this form of addressing benefits several mechanisms of spam control, because group members appear to be users of the group's domain.

Before a group membership is a fact, a group must accept members and members must accept the group.  Once a member is in a group, they gain access to all group services setup under a domain.  This may involve group chat, video meetings, gaming and much more.  There actually is a lot of powerful open source software that implements open protocols for all these use cases.  The one thing that seems to be missing is a general way to group people and regulate their access to such resources; this is where the InternetWide Architecture steps in.

### Why?  Control what is Public

The freedom to talk privately is held in high esteem by most Internet technicians and users; not because they have something to hide, but quite simply because not everything needs to be out in the open.  Most end users see these values disappear rapidly, as they adopt an increasing number of "free" services that are paid for with behaviour-tracking advertisements.  This may be a personal choice, but it is good to realise that asking others to go along with this is at the very least unfriendly.  Sharing personal information without prior consent, such as granting applications access to your contact list, is downright illegal in many countries, and you must not shrug over behaviour that hurts the innate privacy that others may care for.  But you do need technology to do it well, of course.

The technology underpinning online privacy is called encryption, and it works like the secret codes that kids make up; cryptography develops this idea into very strong forms of protection.  The codes that you thought of back then were probably easy to break with a computer, but some of us continued to play the game and got really good at it, even beating computers at this game.

The central idea is always to use publicly known algorithms for scrambling the bits of a message, but use a secret input known as an *encryption key* to make the result irreversible to anyone but the intended recipients.

These keys often get packaged into *certificates* or *public keys* which the InternetWide Architecture habitually generates for any primary identity created (but they also support their aliases).  Moreover, once generated they are published under your domain name using standardised facilities, so anyone can use them when addressing you.

*The ARPA2 project "Keyful Identity Protocol" frees the sender from the need to locate certificates for all recipients, as will be explained later.*

*The ARPA2 project "Remote PKCS #11" allows access to managed private keys from a variety of platforms, such as desktop, laptop and mobile.*

### Why?  Identification and Access Control

It's fun to create identities, but it is essential that only you can express that they are yours.  This expression of control is what *authentication* is about: "yes, it is me".  This line of statement precedes the question of *authorisation*, which is about "are you allowed to do this?"

For authentication, you need to log in.  This is always unpleasant, and should be minimally required.  The best user experience to be had is called *single sign-on*, where you login once a day and continue to use the authenticated identity throughout the day.  For our identity model, that involves all aliases and group memberships (only pseudonyms are distinct identities that require separate login).

With the managed identities of the InternetWide Architecture, it is easy to coordinate accounts with Kerberos.  This system allows just what we want: Login once a day and use its credentials with virtually all protocols you might want to use.

To allow getting on board gradually, we do support password mechanisms, through SASL authentication.  Though passwords are terrible for security reasons, they are a common usage pattern and can be used.  They do not have the benefit of single sign-on, so you may want to migrate to Kerberos just for that.

Every resource and every communication attempt in the InternetWide Architecture is subjected to *access control*, which is the technology imposing authorisation.  Access control is dictated by administrators of a service, resource or communication endpoint.  This is a complex way of saying that you get to control usage patterns under a refined model with sensible defaults.

### Why?  Bring Your Own IDentity

![Bring Your Own IDentity: Let foreign services ask you to control your access (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-2.png)

The principle of creating your own identities is nice, but that is not the same thing as using it everywhere.  An important goal of the InternetWide Architecture is to allow you to *BYOID: Bring Your Own IDentity*.

This means that foreign domains must have a way to trust yours.  A domain owner controls sub-domains and users, so that part can be relied upon, but only when confirmed statements about these identities are made by the domain.  This requires trust couplings which, as a technical term, we have called *Realm Crossover.*  We have found two broad mechanisms of doing this.

*Kerberos Crossover* or KXOVER is a mechanism that connects two domains through their key distribution center (KDC).  It involves the secure techniques TLS, DNSSEC and DANE.  It requires each of these to be safe from Quantum Computers, which is not a current fact of these protocols, but it is nonetheless anticipated.

*SASL Crossover* or SXOVER is a mechanism that allows a foreign server to link back to a client's domain to check a client identity.  This is done over Diameter, a protocol you may not have heard of, but that perfectly fills the gap that we need here.  As with KXOVER, the security of Diameter rests on TLS, DNSSEC and DANE.

Many other protocols can be based on these, including OpenID Connect and OAuth2.  Among the many, many systems designed with the blind-fold of web-only use cases, we still have to find one that we cannot facilitate from the identities setup with the InternetWide Architecture.

### Why?  Control the Future of Online Freedom

Your online freedom may be more fragile than you are aware of.  Any place where you are forced to use one particular piece of software robs you from the freedom to substitute another.  Full control over a piece of software may mean full control over your devices, and all the data stored on it.  This can be so commercially attractive for a vendor that they offer services at no (monetary) cost.

The opposite to this development is the movement towards open protocols and open documents.  This means that the patterns of online exchanges are publicly documented, and that care has been taken to allow multiple interoperable implementations.  Open source is exceptionally attractive for people with a technical inclination, because they may expand and improve the ideas.  And for non-technical users, this means that they can always ask some technical person to make an extension or improvement, or they could switch to other software that suits their needs.  Your online freedom is guaranteed by open protocols and open document formats, nothing ele.

As soon as you need to force software upon others to allow them to communicate with you, you are helping to spread the terror of closed-down lock-in systems.  Such software usually comes shrink-wrapped with privacy policies and a jurisdiction that someone else feel uneasy.  Do not make this mistake; instead of asking your friends to join you on a particular chat service, you should be asking them to use an open protocol such as XMPP so that you may connect to them.  All they need then, is your identity, perhaps one located under your own domain.  It is up to them what software to use, what domain and user identities to setup, and so on.  Similarly, instead of forcing your text processor's file format on them, you should export in a standard format that any bit of software can use, so they can open it with theirs.  Do not be a conformist, but be a mindful user of the Internet and spread openness rather than locking down the most important means of communication that we have, by surrendering your social online presence to self-serving commercial pressure.  Finally, after many years of centralised domination by commercial forces we have an option to be free; that is not something to sacrifice too lightly!

### Why?  Diversity is a Treasure Trove

![Reach Out to find things Just Right for you (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-5.png)

Open source and open protocols is a treasure trove of choice.  However, when facing choices, some people develop a feeling of anxiety.  This is especially the case when they lack the motivation to select from among the alternatives.  The general advise is to *try something, but do not forget that you can always try another option in the future.*

There is no perfect tool for everyone.  Some want the most concise control, and can handle detailed instructions and reports.  Others, usually those new to a situation, are easily confused and prefer to be shown the broad outline and want most details to be handled under reasonable assumptions.  Full control can be just as discouraging as it is empowering.  Where you stand is your personal choice, and it may vary between tools.

Software is a personal choice, and should always be that way.  We need diversity, so lock-down to a one-size-fits-all is pure poverty.  Instead, we should embrace the freedom of source that comes from open document formats and open protocols.

### Why?  Regain your Personal Freedom

Just to make sure that you recognise the current trends that take away your online freedom and that may at worst be threats to the Internet as a whole, please consider the following dangers.

Free services are not necessarily bad, but they raise questions.  Advertisements as a model of making money is perhaps the worst; these models require tracking your online behaviour to the benefit of the advertisers, who may confront you with offers that serve their interests, not yours.  They are downright dangerous to your privacy.  More importantly, someone else decided on pushing this down your throat.  Included in these problems are services willing to process your data, which they may scan to learn more about you for targeted advertising.  Do you want the consumerist's lure of purchases that you would not have made if you had the initiative?  Do you like that advertisements, automatically selected to match your personal browsing behaviours, can popup when you are showing something to others on your screen?  Most people would agree that they desire more control.

Closed protocols and closed document formats are a problem, as explained before; they allow forcing software upon you.  This is usually an even more interesting way to tap your data.  Mobile phones are habitually filled with apps that cannot be properly validated (is this my bank, or something that uses its name?) and that grab more control over your phone use than they strictly require.  Contrast that with a website that simply lets you browse for data, or chat services that you can connect to with your personal choice of software.  Even when you prefer to use their offering, it remains a good question whether there are alternatives that would also work, and not be offered by the respective vendor.

*How many chat, videoconferencing and other communication tools do you have?  And how compatible are all these tools?*  Diversity may be a treasure trove, but only if you get to choose.  Are others already telling you what software to use, and do you need a variety of tools to communicate with everyone?  That's a first-hand experience of the trouble that comes from proprietary lock-in solutions.  This is silly because, with the exception of very few cutting-edge ideas, these closed solutions tend to have perfectly usable open protocols doing exactly the same!  Stop trying to convince your friends about your personal choice of software and instead make it worth everyone's effort, by convincing them about the advantages of moving towards open solutions that allow everyone their free choice of user interface.  Try to be part of the solution, not to worsen the problem of proprietary lock-in software.

Websites are popular, but they are often part of this sad pattern.  You might think of the web as an open protocol, but this is really just true for the basic nuts and bolts; it is typical of "web apps" that they lock you into a particular bit of software; you just don't notice it because it comes as JavaScript that travels alongside the data.  For web services, the best you might get is an open specification for the interaction and a general open source software product.  This problem does not apply to all environments, but to many.

Moving away from the web to semantically richer protocols is a much better choice.  Specifically because it gets you away from JavaScript, which can be loaded dynamically from a variety of sites, including as part of advertisements.  Richer protocols are not always available, but the patterns of human social behaviour are pretty static and open protocols to support those exist much more often than you would think.  An important step to take is to stop thinking that everything should run from within your browser; most applications get much better when they move far away from the browser!  As you may understand from this, you can find a choice of non-web protocols in the InternetWide Architecture.  The web has its value and is included, but should not be considered basic infrastructure.  For many purposes there exist better protocols, which offer rich facilities that are custom-designed to their purpose.  When an open protocol has a public and complete specification, it usually comes with a range of open source implementations to choose from.  For the specific purposes of the InternetWide Architecture we sometimes needed to make extensions, but these are *always openly and completely documented to enable alternative implementations*.  So, even if we do provide software implementations, then this is merely to enable you to use our ideas, but we are happy to see others make their own implementations of the same ideas.


## What to do: The Structure of Control

We shall now describe the building blocks of the InternetWide Architecture, and how they help you control your domain.  *This explains what to consider.  Look above for why, and turn to the next section for details about how to do it.*


![Long Codes are Keys (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-1.png)


### What?  Helm Arbitrary Access Nodes

HAAN Service is downright simple.  You talk to it and get passed a username and password.  They are long random codes and so they are unique.  *The HAAN Service has no need to store your credentials* so they cannot be stolen.  Instead, a mathematical relation between the username and password allows the HAAN Service to check your identity at a later time.  Nobody but the same HAAN Service can do this; the relation involves the use of a secret key that is kept in safe keeping in the HAAN Service.

The idea of HAAN is to make it easy for anyone to get an identity.  And to make that an incredibly light-weight service, so virtually anyone could create this kind of identity.  You get to choose the parties that you want to entrust with the task of defining this identity; you should look at their ideology or you could run your own HAAN Service on your own router.

This kind of identity is great for fallback purposes, and is much more secure than the current practice of password reset links being mailed to an email address (to which you may not have the password anymore either...).  For this purpose, it is helpful if you have a number of alternative options.  One in an envelope in a safe, another in the spine of a book that is too boring to read, and maybe another kept by someone who you blindly trust.

Here's an example of the kind of identity you might get from a HAAN Service:

```
Username: 64CIEHK3Q3FTKMSK5KBYOMH2XBJH7SK5
Realm:    helm.arpa2.org
Password: OYYBFQ4PDKP25JGSRCBFJBZS7PJSG76D
```

These codes are not made to be friendly, but copy/paste is patient and instead of entering them every day you will only enter them in exceptional cases.  Instead of being friendly, they are designed to be secure enough to support end-users who landed in access recovery problems.  Without a need to tamper with security, they even leave some headroom for corrections in case you registered it with typos:

Old-fashioned (web) services would only allow you to use such an identity in the indicated realm, but we have worked on Realm Crossover to allow any service to connect to this realm to check whether it's you trying to get in.  This cheap and easy identity is in reality an InternetWide Identity!

These identities have no value on their own; nobody cares if you dispose of them.  Only by registering the username and realm as your identity are you assigning value to them.  The codes are long and will be tedious if you ever need to enter them by hand, but that same property makes them extremely difficult to guess by a potential attacker.  Even if they ever got hold of the secret key backing the HAAN Service, they still could not guess the username (and derive the password) that were assigned to you.  This is why you might consider storing this rescue information offline, and not on your computer.

You can already see the mountain of options to use this.  Don't get discouraged though.  *This treasure trove of options is really simple to use.*  Just find a suitable HAAN Service and download a fresh Helm Key.  Then rinse and repeat at other HAAN Service.


### What?  Sit at the Helm

The credentials from HAAN Service can be used on other realms, so long as they support Realm Crossover.  The idea is that providers who support the InternetWide Architecture present you with an interface known as the Helm, which serves as your entry point to recovery and control of online resources like domain names and email addresses.  The credentials provided by a HAAN Service would serve as your Helm Key.  You may add new Helm Keys for the same Helm, remove ones, and control what other Helm Keys may access these online resources.  In short, you are sitting at the Helm of your online resources.

Helm Keys are in general any credential that can be validated with Realm Crossover.  The important thing is that it can be any user name defined under any domain name (or "secure realm") and that the Helm will connect there to verify the access.  There is no reason why you could not give a user identity like `admin+john@example.com` access by permitting their identity to unlock your Helm.  What a HAAN Service provides is just for bootstrapping your online presence without John's help, and for allowing you to have completely indifferent and independent identities to bootstrap multiple online resources.

The best thing you can do is create your own Helm at a domain provider, setup Helm Keys as your backup access and then register a domain name controllable from your Helm.  This may suggest you to register a more easily remembered identity for control, such as `admin@domain.name` under your own `domain.name` registration.  That way, when your domain has a role by that name it can be used to contol the domain "from within".  This works because your domain facilitates Realm Crossover and if any domain can access your domain's identities, then so can your domain itself.

Your domain's users can also be saved from the burden of password recovery emails if they have their own Helm with Helm Keys.  They request a username under your domain from this personally controlled Helm.  They would also be able use HAAN Service to bootstrap their online presence.

In general, a Helm Key makes sense upon entry of an online resource, be it a domain name or email address or anything else.  For derived identities, it makes more sense to grant access to the original identity.  If you are sometimes a domain admin and sometimes a regular user, it may make sense to create a separate account just for your everyday use as a regular user.  This will avoid that you carry around the full potential of domain management during your everyday activities as a user.


### What?  Wearing many Hats

As soon as you own a domain name, you can create user names.  These can be used for login to other domains as well, under the assumption that your hosting provider implements Realm Crossover, and a contacted service provides it.  But do you want to present the same identity everywhere?  There are many reasons to refine this and wear many hats.  The pivotal point for switching to an alias would be when you crossover to another domain.

You might like to use separate identities for your different (online) activities.  You may be a gardener, guitarist and gamer; having a separate identity for each could help you to sort incoming communications (such as email or chat) on each of these perspectives.  Not only is it helpful for you; it could also simplify spam filtering.  And, you might be more or less stringent about who may communicate with you on each of the roles.

The identity system of the InternetWide Architecture enables light-weight identities for such purposes.  For a user `john@example.com`, they might look like `john+gardener@example.com`, `john+guitarist@example.com` and `john+gamer@example.com`, for example.  These are called aliases, and come with their own Access Control List for Communication.  You could have an identity such as `info@example.com` open to all that you read only occasionally, and others that only few can reach so you can keep up with them, especially when they concern what you are presently doing (work versus play, for instance).

If someone gave away your alias identity without your permission (perhaps by using a privacy-breaching App that extracts and uploads your data from their phone) then a third party might not be able to contact you there.  A web shop might feel a desire to contact you with a lure after a deal has ended.  In all those cases, you could take control by blocking such access to your communication.

There are more extreme examples of control; we facilitate special identities that carry with them a code to check on information from the context.  Think of a sender's domain name or full contact address.  Think of the subject line, or perhaps a `[topic]` or `[12345]` number that you require to occur in it.  You can even set an expiration date on this special identity format!

All aliases are reached from the same login, in the example cases as `john@example.com`.  You might also have other logins, which is a more heavy-weight mechanism requiring separate login.  An example of that is `admin@example.com` which may grant completely different rights.

### What?  Flocking and Herding

![Collaboration needs Cryptography to Unlock Trust (art by Moniek Rutten)](img/moniek.rutten-indexed-smaller-3.png)

To collaborate with others, you probably want to form groups.  You can create groups under your domain name, and they look just like ordinary users, for instance `gamer@example.com`.  If John signs up to this group (and is accepted) then he can choose a membership name to be appended.  He might be `gamer+john@example.com` or, if he wants, he could be `gamer+darthvader@example.com` instead.

When using the group, John's identity will appear as his group member name.  The rights to do things in the group will be setup in an Access Control List, and might allow any `gamers+` to do certain things, while other things may be just for `gamers+mary@example.com` so John cannot touch that.  All this can be tweaked if default settings don't work out.  (Tweaks would also be subjected to an Access Control List, to define who is an effective group master.)

Note that we didn't say anything about services yet.  The intention of the InternetWide Architecture is where *all services support all your identities* &mdash; and that includes groups.  So if you have write access to a group, then services might grant you the rights to upload documents to a group's reservoir, might publish on the group's website, might organise meetings, initiate group calls, and so on.  The service decides on the precise meaning of the various rights of members, but all services recognise the same group members.

Something else worth mentioning is that groups can welcome external members.  They too get a membership name, so `gamer+whiteknight@example.com` may be a user of another domain.  It is still an authenticated user &mdash; we simply employ Realm Crossover.  And yes, it might be an identity provided by a HAAN Service, if mere authentication suffices.


## TODO: HOW

*We're not that concrete yet.  We hope to add a step-by-step tutorial here, but our software is still too much in flux.*
